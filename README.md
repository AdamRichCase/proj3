# Purpose
To show the implementation of docker, flask, and AJAX to create a anagram game.

# Authors

* Initial version by M Young;
* Docker version added by R Durairajan
* AJAX implemented by Adam Case

# Status

flask_vocab.py and the template vocab.html are a 'skeleton' version of the anagram game for a CIS 322 project. They uses conventional interaction through a form, interacting only when the user submits the form.

# Usage

* Use the ```docker build <image-name> .``` in the /vocab directory to create the image
* After making the image, run using ```docker run -d -p <host-port>:5000 <image-name>```
* Access through ```localhost:<host-port>```
